from django.shortcuts import render
from django.http import HttpResponse
from .models import Organization, Work
from django.template import loader


def index(request):
    num_work = Work.objects.all()
    num_organization = Organization.objects.all()
    template = loader.get_template('home.html')
    context = {'num_work': num_work, 'num_organization': num_organization}
 #   return render(request, 'home.html', context={'num_work': num_work, 'num_organization': num_organization}, )

    return HttpResponse(template.render(context, request))
