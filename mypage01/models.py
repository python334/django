from django.db import models

class Organization(models.Model):
    title = models.CharField(max_length=200)
    def __str__(self):
        return self.title

class Work(models.Model):
    datefrom = models.DateTimeField('date published')
    dateto = models.DateTimeField('date published')
    firm = models.ForeignKey(Organization, on_delete=models.CASCADE)
    description = models.CharField(max_length=200)

# class Hobbies(models.Model):
#     id = models.Field(primary_key = True)
#     description = models.CharField(max_length=200)
#     datefrom = models.DateTimeField()
#
# class Education(models.Model):
#     id = models.Field(primary_key = True)
#     description = models.CharField(max_length=200)
#     datefrom = models.DateTimeField()
#     dateto = models.DateTimeField()
#     educational_institution = models.ForeignKey(Organization, on_delete=models.CASCADE)
#
# class FavoriteActivity(models.Model):
#     id = models.Field(primary_key = True)


