from django.contrib import admin
from .models import Organization, Work

admin.site.register(Organization)
admin.site.register(Work)